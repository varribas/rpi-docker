#!/bin/bash

set -e

echo "Using the followin' environment setup ..."
echo "env.NEXTCLOUD_DIFFIE_HELLMAN is $NEXTCLOUD_DIFFIE_HELLMAN"
echo "env.NEXTCLOUD_VERSION is $NEXTCLOUD_VERSION"
echo "env.NEXTCLOUD_BASE_DIR is $NEXTCLOUD_BASE_DIR"
echo "env.NEXTCLOUD_CERT_DIR is $NEXTCLOUD_CERT_DIR"
echo "env.NEXTCLOUD_DATA_DIR is $NEXTCLOUD_DATA_DIR"
echo "env.NEXTCLOUD_CONFIG_DIR is $NEXTCLOUD_CONFIG_DIR"
echo "env.NEXTCLOUD_SERVERNAME is $NEXTCLOUD_SERVERNAME"

if [ -z "$NEXTCLOUD_SERVERNAME" ]; then
    echo >&2 'error: you have to provide a server-name'
    echo >&2 '  Did you forget to add -e NEXTCLOUD_SERVERNAME=... ?'
    exit 1
fi

if [ ! -d "$NEXTCLOUD_DATA_DIR" ]; then
    echo >&2 "error: there is no data-dir available!!"
    echo >&2 "   Are you sure you've mounted a volume from the data container??"
    exit 2
fi

echo "Updating the servers hostname (to $NEXTCLOUD_SERVERNAME)"
sudo sed -i "s/server_name localhost/server_name $NEXTCLOUD_SERVERNAME/g" /etc/nginx/sites-available/default

if [ ! -f "$NEXTCLOUD_CONFIG_DIR/configuration.done" ]; then
    echo "Creating configuration in $NEXTCLOUD_CONFIG_DIR"

    sudo cp /tmp/autoconfig.tmpl $NEXTCLOUD_CONFIG_DIR/autoconfig.php
    sudo sed -i "s@\"directory\".*,\$@\"directory\" => \"$NEXTCLOUD_DATA_DIR\",@g" $NEXTCLOUD_CONFIG_DIR/autoconfig.php

    echo "Setting up db credentials ($NEXTCLOUD_DB_USER/********)"
    sudo sed -i "s@\"dbuser\".*,\$@\"dbuser\" => \"$NEXTCLOUD_DB_USER\",@g" $NEXTCLOUD_CONFIG_DIR/autoconfig.php
    sudo sed -i "s@\"dbpass\".*,\$@\"dbpass\" => \"$NEXTCLOUD_DB_PASSWORD\",@g" $NEXTCLOUD_CONFIG_DIR/autoconfig.php
    sudo touch $NEXTCLOUD_CONFIG_DIR/configuration.done
fi

# prepare the stage for letsencrypt requests
if [ -d "$NEXTCLOUD_DATA_DIR" ] && [ ! -d "$NEXTCLOUD_DATA_DIR/letsencrypt" ]; then
    echo "Creating the directory where letsencrypt will store its challanges ..."
    sudo mkdir -p $NEXTCLOUD_DATA_DIR/letsencrypt
fi

if [ ! -f "$NEXTCLOUD_CERT_DIR/nextcloud.pem" ]; then
    echo "Generating self-signed certificates ..."
    echo "Notice: You can overwrite these 'untrusted' certificates with a trausted letsencrypt"
    echo "        certificate - just use the schoeffm/rpi-dehydrated image!"

    sudo mkdir -p $NEXTCLOUD_CERT_DIR
    sudo openssl genrsa -out $NEXTCLOUD_CERT_DIR/nextcloud.key 4096 
    sudo openssl req -new -sha256 -batch -subj "/CN=$NEXTCLOUD_SERVERNAME" -key $NEXTCLOUD_CERT_DIR/nextcloud.key -out $NEXTCLOUD_CERT_DIR/nextcloud.csr 
    sudo openssl x509 -req -sha256 -days 3650 -in $NEXTCLOUD_CERT_DIR/nextcloud.csr -signkey $NEXTCLOUD_CERT_DIR/nextcloud.key -out $NEXTCLOUD_CERT_DIR/nextcloud.pem
fi


# in order to activate Diffie-Hellman for TLS
if [ ! -f "$NEXTCLOUD_CERT_DIR/dhparam.pem" ] && [ "$NEXTCLOUD_DIFFIE_HELLMAN" == "on" ]; then
    echo "env.NEXTCLOUD_DIFFIE_HELLMAN is $NEXTCLOUD_DIFFIE_HELLMAN"
    echo "Will generate a 2048-bit long Diffie-Hellman Params File (take a 20 minute break)"    

    # on rpi this comman will take a huge period of time ... so be patient
    sudo openssl dhparam -out $NEXTCLOUD_CERT_DIR/dhparam.pem 2048
    sudo sed -i "s@# ssl_dhparam@ssl_dhparam $NEXTCLOUD_CERT_DIR/dhparam.pem;@g" /etc/nginx/sites-available/default
fi

exec "$@"

FROM schoeffm/rpi-nginx-php5
MAINTAINER Stefan Schoeffmann <stefan.schoeffmann@posteo.de>

ENV NEXTCLOUD_VERSION=nextcloud-11.0.2 \
    NEXTCLOUD_BASE_DIR=/var/www \
    NEXTCLOUD_CERT_DIR=/srv/http/ssl \
    NEXTCLOUD_DATA_DIR=/srv/http/nextcloud/data \
    NEXTCLOUD_CONFIG_DIR=/var/www/nextcloud/config \
    NEXTCLOUD_SERVERNAME=dockerpi \
    NEXTCLOUD_DIFFIE_HELLMAN=on \
    NEXTCLOUD_DB_USER=nextcloud \
    NEXTCLOUD_DB_PASSWORD=mycloud \
    NEXTCLOUD_STORAGE_BASE_DIR=/var/www/external

WORKDIR "$NEXTCLOUD_BASE_DIR"

# Change UID ang GID for www-data in order to write files properly
RUN usermod -u 1000 www-data && groupmod -g 1000 www-data

RUN apt-get update && apt-get install -y wget bzip2 vim && \
    sed -i 's/\;env\[/env\[/g' /etc/php5/fpm/pool.d/www.conf && \
    sed -i "/env\[PATH\]/c\env\[PATH\] = "`printenv PATH` /etc/php5/fpm/pool.d/www.conf && \
    sed -i "s/upload_max_filesize = 2M/upload_max_filesize = 2048M/g" /etc/php5/fpm/php.ini && \
    sed -i "s/post_max_size = 8M/post_max_size =root123  2048M/g" /etc/php5/fpm/php.ini && \            
    echo 'default_charset = "UTF-8"' >> /etc/php5/fpm/php.ini && \
    echo "upload_tmp_dir = $NEXTCLOUD_DATA_DIR" >> /etc/php5/fpm/php.ini && \
    echo "extension = apc.so" >> /etc/php5/fpm/php.ini && \
    echo "apc.enabled = 1" >> /etc/php5/fpm/php.ini && \
    echo "apc.include_once_override = 0" >> /etc/php5/fpm/php.ini && \
    echo "apc.shm_size = 256" >> /etc/php5/fpm/php.ini && \    
    wget https://download.nextcloud.com/server/releases/"$NEXTCLOUD_VERSION".tar.bz2 && \
    tar xvf "$NEXTCLOUD_VERSION".tar.bz2 && \
    rm "$NEXTCLOUD_VERSION".tar.bz2 && \
    chown -R www-data:www-data nextcloud && \
    mkdir -p "$NEXTCLOUD_STORAGE_BASE_DIR" && \
    chown www-data:www-data "$NEXTCLOUD_STORAGE_BASE_DIR"    

ADD default /etc/nginx/sites-available/default
ADD autoconfig.php /tmp/autoconfig.tmpl

WORKDIR /
COPY docker-entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD service php5-fpm start && nginx
